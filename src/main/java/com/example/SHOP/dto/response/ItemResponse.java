package com.example.SHOP.dto.response;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemResponse {

    private Long itemId;
    private String itemName;
    private Integer itemStock;
    private Double itemPrice;
    private Timestamp itemLastRestock;
    private Boolean isAvailable;
    private Boolean isDeleted;
    
}
