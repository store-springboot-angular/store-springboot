package com.example.SHOP.dto.response;

import java.util.List;

import com.example.SHOP.dto.pagination.OrderPagination;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderListResponse {

    private List<OrderResponse> data;
    private OrderPagination pagination;
    
}
