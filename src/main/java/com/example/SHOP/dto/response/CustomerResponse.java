package com.example.SHOP.dto.response;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerResponse {
    
    private Long customerId;
    private String customerName;
    private String customerAddress;
    private String phoneNumber;
    private Boolean isActive;
    private Timestamp lastOrder;
    private String picture;

}
