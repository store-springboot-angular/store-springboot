package com.example.SHOP.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateCustomerResponse {

    private String customerName;
    private String customerAddress;
    private String phoneNumber;
    private String message;
    
}
