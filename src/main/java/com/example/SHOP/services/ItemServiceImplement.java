package com.example.SHOP.services;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.SHOP.dto.request.AddItemRequest;
import com.example.SHOP.dto.request.ItemRequest;
import com.example.SHOP.dto.request.UpdateItemRequest;
import com.example.SHOP.dto.response.ItemResponse;
import com.example.SHOP.models.ItemModel;
import com.example.SHOP.repositories.ItemRepository;

@Service
public class ItemServiceImplement implements IItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public Page<ItemModel> findAllItems(Pageable pageable) {
        return itemRepository.findAllByIsDeletedFalse(pageable);

    }

    @Override
    public Page<ItemModel> findItemsByName(String itemName, Pageable pageable) {
        return itemRepository.findByItemNameContainingIgnoreCase(itemName, pageable);
    }

    @Override
    public List<ItemResponse> convertToItemResponse(Page<ItemModel> itemModels) {
        return itemModels.stream().map(this::mapToItemResponse).collect(Collectors.toList());
    }

    private ItemResponse mapToItemResponse(ItemModel itemModels) {
        return new ItemResponse(
                itemModels.getItemId(),
                itemModels.getItemName(),
                itemModels.getItemStock(),
                itemModels.getItemPrice(),
                itemModels.getItemLastRestock(),
                itemModels.getIsAvailable(),
                itemModels.getIsDeleted());
    }

    @Override
    public ItemResponse findByItemByItemId(ItemRequest itemRequest) {
        Long itemId = itemRequest.getItemId();
        ItemModel existingItem = itemRepository.findById(itemId)
                .orElseThrow(() -> new IllegalArgumentException("Item not found with id: " + itemId));

        if (existingItem.getIsDeleted()) {
            throw new IllegalArgumentException("Item with ID " + itemId + " is deleted.");
        }

        return mapToItemResponse(existingItem);
    }

    @Override
    public ItemModel addItem(AddItemRequest addItemRequest) {
        addItemRequest.validate();
        ItemModel itemModel = new ItemModel();
        itemModel.setItemName(addItemRequest.getItemName());
        itemModel.setItemPrice(addItemRequest.getItemPrice());
        itemModel.setItemStock(addItemRequest.getItemStock());
        itemModel.setItemLastRestock(Timestamp.valueOf(LocalDateTime.now()));
        itemModel.setIsAvailable(true);
        itemModel.setIsDeleted(false);

        return itemRepository.save(itemModel);
    }

    @Override
    public ItemModel updateItem(Long itemId, UpdateItemRequest updateItemRequest) {
        updateItemRequest.validate();

        ItemModel existingItem = itemRepository.findById(itemId)
                .orElseThrow(() -> new IllegalArgumentException("Item not found with id: " + itemId));

        if (existingItem.getIsDeleted()) {
            throw new IllegalArgumentException("Item with ID " + itemId + " is deleted.");
        }

        existingItem.setItemName(updateItemRequest.getItemName());
        existingItem.setItemPrice(updateItemRequest.getItemPrice());
        existingItem.setItemStock(updateItemRequest.getItemStock());
        existingItem.setIsAvailable(existingItem.getItemStock() > 0);

        if (existingItem.getItemStock() > 0) {
            existingItem.setItemLastRestock(Timestamp.valueOf(LocalDateTime.now()));
        }
        return itemRepository.save(existingItem);
    }

    @Override
    public ItemModel deleteItem(Long itemId) {
        ItemModel existingItem = itemRepository.findById(itemId)
                .orElseThrow(() -> new IllegalArgumentException("Item not found with id: " + itemId));
        existingItem.setIsDeleted(true);

        return itemRepository.save(existingItem);
    }

}
