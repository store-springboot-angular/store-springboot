package com.example.SHOP.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.SHOP.dto.request.AddItemRequest;
import com.example.SHOP.dto.request.ItemRequest;
import com.example.SHOP.dto.request.UpdateItemRequest;
import com.example.SHOP.dto.response.ItemResponse;
import com.example.SHOP.models.ItemModel;

@Service
public interface IItemService {

    Page<ItemModel> findAllItems(Pageable pageable);

    Page<ItemModel> findItemsByName(String itemName, Pageable pageable);

    List<ItemResponse> convertToItemResponse(Page<ItemModel> itemModels);

    ItemResponse findByItemByItemId(ItemRequest itemRequest);

    ItemModel addItem(AddItemRequest addItemRequest);

    ItemModel updateItem(Long itemId, UpdateItemRequest updateItemRequest);

    ItemModel deleteItem(Long itemId);
    
}
