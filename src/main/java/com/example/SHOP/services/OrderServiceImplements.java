package com.example.SHOP.services;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.SHOP.dto.request.AddOrderRequest;
import com.example.SHOP.dto.request.OrderRequest;
import com.example.SHOP.dto.request.UpdateOrderRequest;
import com.example.SHOP.dto.response.OrderResponse;
import com.example.SHOP.models.CustomerModel;
import com.example.SHOP.models.ItemModel;
import com.example.SHOP.models.OrderModel;
import com.example.SHOP.repositories.CustomerRepository;
import com.example.SHOP.repositories.ItemRepository;
import com.example.SHOP.repositories.OrderRepository;

@Service
public class OrderServiceImplements implements IOrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public Page<OrderModel> findAllOrders(Pageable pageable) {
        return orderRepository.findAll(pageable);
    }

    @Override
    public Page<OrderModel> findOrdersByCustomerName(String customerName, Pageable pageable) {
        return orderRepository.findByCustomer_CustomerNameContainingIgnoreCase(customerName, pageable);
    }

    @Override
    public List<OrderResponse> convertToOrderResponse(Page<OrderModel> orderModels) {
        return orderModels.stream()

                .map(this::mapToOrderResponse)
                .collect(Collectors.toList());
    }

    private OrderResponse mapToOrderResponse(OrderModel orderModel) {
        return new OrderResponse(
                orderModel.getOrderId(),
                orderModel.getCustomer().getCustomerName(),
                orderModel.getItem().getItemName(),
                orderModel.getQuantity(),
                orderModel.getTotalPrice(),
                orderModel.getOrderDate());
    }

    @Override
    public OrderResponse findByOrderByOrderId(OrderRequest orderRequest) {
        Long orderId = orderRequest.getOrderId();
        OrderModel existingOrder = orderRepository.findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException("Order not found with id: " + orderId));

        return mapToOrderResponse(existingOrder);
    }

    @Override
    public OrderModel addOrder(AddOrderRequest addOrderRequest) {
        addOrderRequest.validate();

        CustomerModel customer = customerRepository.findById(addOrderRequest.getCustomerId())
                .orElseThrow(() -> new IllegalArgumentException(
                        "Customer not found with id: " + addOrderRequest.getCustomerId()));
        if (!customer.getIsActive()) {
            throw new IllegalArgumentException(
                    "Customer with id: " + addOrderRequest.getCustomerId() + " is not active");
        }

        customer.setLastOrder(Timestamp.valueOf(LocalDateTime.now()));

        ItemModel itemModel = itemRepository.findById(addOrderRequest.getItemId())
                .orElseThrow(() -> new IllegalArgumentException(
                        "Item not found with id: " + addOrderRequest.getItemId()));

        if (!itemModel.getIsAvailable()) {
            throw new IllegalArgumentException(
                    "Item with id: " + addOrderRequest.getItemId() + " is not available");
        }
        if (itemModel.getIsDeleted()) {
            throw new IllegalArgumentException(
                    "Item with id: " + addOrderRequest.getItemId() + " is deleted");
        }

        if (itemModel.getItemStock() < addOrderRequest.getQuantity()) {
            throw new IllegalArgumentException("Out of stock for Item with id: " + addOrderRequest.getItemId());
        }
        int newStock = itemModel.getItemStock() - addOrderRequest.getQuantity();
        itemModel.setItemStock(newStock);

        if (newStock == 0) {
            itemModel.setIsAvailable(false);
        }

        OrderModel orderModel = new OrderModel();
        orderModel.setCustomer(customer);
        orderModel.setItem(itemModel);
        orderModel.setQuantity(addOrderRequest.getQuantity());
        orderModel.setTotalPrice(addOrderRequest.getTotalPrice());
        orderModel.setOrderDate(Timestamp.valueOf(LocalDateTime.now()));
        return orderRepository.save(orderModel);
    }

    @Override
    public OrderModel updateOrder(Long orderId, UpdateOrderRequest updateOrderRequest) {
        updateOrderRequest.validate();

        OrderModel existingOrder = orderRepository.findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException("Order not found with id: " + orderId));

        CustomerModel customer = customerRepository.findById(updateOrderRequest.getCustomerId())
                .orElseThrow(() -> new IllegalArgumentException(
                        "Customer not found with id: " + updateOrderRequest.getCustomerId()));

        if (!customer.getIsActive()) {
            throw new IllegalArgumentException(
                    "Customer with id: " + updateOrderRequest.getCustomerId() + " is not active");
        }

        customer.setLastOrder(Timestamp.valueOf(LocalDateTime.now()));


        ItemModel itemModel = itemRepository.findById(updateOrderRequest.getItemId())
                .orElseThrow(() -> new IllegalArgumentException(
                        "Item not found with id: " + updateOrderRequest.getItemId()));

        if (itemModel.getIsDeleted()) {
            throw new IllegalArgumentException(
                    "Item with id: " + updateOrderRequest.getItemId() + " is deleted");
        }

      
        int itemId = existingOrder.getItem().getItemId().intValue();
        int itemIdRequest = updateOrderRequest.getItemId().intValue();
        int newStock = 0;
        int oldStockOrder = existingOrder.getQuantity();
        int requestStock = updateOrderRequest.getQuantity();
        int stockItem = itemModel.getItemStock();

        if (itemIdRequest != itemId) {
          int existingItemStock= existingOrder.getItem().getItemStock(); 
          int existingQuantity =  existingOrder.getQuantity();
            int newStockItem = existingItemStock + existingQuantity;
            existingOrder.getItem().setItemStock(newStockItem);
        }

        if (requestStock < oldStockOrder) {
            newStock = (oldStockOrder - requestStock) + stockItem;
        } else if (requestStock > oldStockOrder) {
            newStock = stockItem - (requestStock - oldStockOrder);
        } else if (requestStock == oldStockOrder) {
            newStock = stockItem;
        }

        if (newStock < 0) {
            throw new IllegalArgumentException(
                    "Out of stock for Item with id: " + updateOrderRequest.getItemId());
        }

        itemModel.setItemStock(newStock);

        if (newStock == 0) {
            itemModel.setIsAvailable(false);
        } else if (newStock > 0) {
            itemModel.setIsAvailable(true);
        }

        existingOrder.setCustomer(customer);
        existingOrder.setItem(itemModel);
        existingOrder.setQuantity(updateOrderRequest.getQuantity());
        existingOrder.setTotalPrice(updateOrderRequest.getTotalPrice());
        existingOrder.setOrderDate(Timestamp.valueOf(LocalDateTime.now()));

        return orderRepository.save(existingOrder);
    }

    @Override
    public OrderModel deleteOrder(Long orderId) {
        OrderModel existingOrder = orderRepository.findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException("Order not found with id: " + orderId));

                existingOrder.getItem().setItemStock(existingOrder.getItem().getItemStock() + existingOrder.getQuantity());
                existingOrder.getItem().setIsAvailable(true);
        orderRepository.deleteById(orderId);
        return existingOrder;
    }
}
