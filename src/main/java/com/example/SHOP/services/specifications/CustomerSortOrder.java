package com.example.SHOP.services.specifications;

import org.springframework.data.domain.Sort;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CustomerSortOrder {

    CUSTOMER_NAME_ASC("customerName", Sort.Direction.ASC),
    CUSTOMER_NAME_DESC("customerName", Sort.Direction.DESC),
    CUSTOMER_ADDRESS_ASC("customerAddress", Sort.Direction.ASC),
    CUSTOMER_ADDRESS_DESC("customerAddress", Sort.Direction.DESC),
    PHONE_NUMBER_ASC("phoneNumber", Sort.Direction.ASC),
    PHONE_NUMBER_DESC("phoneNumber", Sort.Direction.DESC),;
    
    private final String fieldName;
    private final Sort.Direction direction;
}
