package com.example.SHOP.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.SHOP.models.ItemModel;

@Repository
public interface ItemRepository extends JpaRepository<ItemModel, Long> {

    Page<ItemModel> findAllByIsDeletedFalse(Pageable pageable);

    Page<ItemModel> findByItemNameContainingIgnoreCase(String itemName, Pageable pageable);

    Optional<ItemModel> findByItemId(Long itemId);
}
