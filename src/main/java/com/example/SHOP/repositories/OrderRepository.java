package com.example.SHOP.repositories;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.SHOP.models.OrderModel;

@Repository
public interface OrderRepository extends JpaRepository<OrderModel, Long> {

    Page<OrderModel> findByCustomer_CustomerNameContainingIgnoreCase(String customerName, Pageable pageable);

    Optional<OrderModel> findByOrderId(Long orderId);
}
