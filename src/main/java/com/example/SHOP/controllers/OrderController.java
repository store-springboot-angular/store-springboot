package com.example.SHOP.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.SHOP.dto.pagination.ItemsInfo;
import com.example.SHOP.dto.pagination.OrderPagination;
import com.example.SHOP.dto.request.AddOrderRequest;
import com.example.SHOP.dto.request.OrderRequest;
import com.example.SHOP.dto.request.UpdateOrderRequest;
import com.example.SHOP.dto.response.OrderListResponse;
import com.example.SHOP.dto.response.OrderResponse;
import com.example.SHOP.models.OrderModel;
import com.example.SHOP.services.IOrderService;
import com.example.SHOP.services.specifications.OrderSortOrder;

@RestController
@RequestMapping("/")
public class OrderController {

    @Autowired
    private IOrderService iOrderService;

    @GetMapping("orders")
    public ResponseEntity<?> getAllOrders(@RequestParam(required = false) String customerName,
            @RequestParam(required = false) OrderSortOrder sortOrder,
            @RequestParam(defaultValue = "1") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        Sort sort;
        if (sortOrder != null) {
            sort = Sort.by(Sort.Order.by(sortOrder.getFieldName()).with(sortOrder.getDirection()));
        } else {
            sort = Sort.by(Sort.Order.by("customer.customerName").with(Sort.Direction.ASC));
        }
        Pageable paging = PageRequest.of(pageNo - 1, pageSize, sort);
        Page<OrderModel> pageResult;

        if (customerName != null && !customerName.isEmpty()) {
            pageResult = iOrderService.findOrdersByCustomerName(customerName, paging);
        } else {
            pageResult = iOrderService.findAllOrders(paging);
        }

        List<OrderResponse> orderResponse = iOrderService.convertToOrderResponse(pageResult);
        ItemsInfo itemsInfo = new ItemsInfo(
                orderResponse != null ? orderResponse.size() : 0,
                (int) pageResult.getTotalElements(),
                pageSize);

        int lastVisiblePage = pageResult.getTotalPages() > 0 ? pageResult.getTotalPages() : 1;

        OrderPagination paginationInfo = new OrderPagination(
                pageNo,
                pageResult.hasNext(),
                itemsInfo,
                lastVisiblePage);

        OrderListResponse response = new OrderListResponse(orderResponse, paginationInfo);

        if (orderResponse == null || orderResponse.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("order/{orderId}")
    public ResponseEntity<Object> getOrderById(@PathVariable Long orderId) {
        OrderRequest orderRequest = new OrderRequest(orderId);
        OrderResponse order = iOrderService.findByOrderByOrderId(orderRequest);

        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("Order", order);
        responseBody.put("message", "Order with ID " + orderId + "Found");
        return ResponseEntity.ok(responseBody);

    }

    @PostMapping("order")
    public ResponseEntity<OrderModel> addOrder(@RequestBody AddOrderRequest addOrderRequest,

            UriComponentsBuilder uriBuilder) {
        OrderModel orderModel = iOrderService.addOrder(addOrderRequest);

        UriComponents uriComponents = uriBuilder.path("/order/{orderId}")
                .buildAndExpand(orderModel.getOrderId());
        return ResponseEntity.created(uriComponents.toUri()).body(orderModel);
    }

    @PutMapping("order/{orderId}")
    public ResponseEntity<Object> updateOrder(@PathVariable Long orderId,
            @RequestBody UpdateOrderRequest updateOrderRequest) {

        OrderModel updateOrder = iOrderService.updateOrder(orderId, updateOrderRequest);

        return ResponseEntity.ok(updateOrder);
    }

    @DeleteMapping("order/{orderId}")
    public ResponseEntity<Map<String, Object>> deleteOrder(@PathVariable Long orderId) {
        iOrderService.deleteOrder(orderId);
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("path", "/orders/" + orderId);
        responseBody.put("error", "Success");
        responseBody.put("message", "Order with ID " + orderId + " has been successfully deleted.");
        responseBody.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        responseBody.put("status", HttpStatus.OK.value());
        return ResponseEntity.ok(responseBody);
    }
}
