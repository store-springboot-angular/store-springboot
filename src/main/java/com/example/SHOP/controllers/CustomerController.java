package com.example.SHOP.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.SHOP.dto.pagination.CustomerPagination;
import com.example.SHOP.dto.pagination.ItemsInfo;
import com.example.SHOP.dto.request.AddCustomerRequest;
import com.example.SHOP.dto.request.CustomerRequest;
import com.example.SHOP.dto.request.UpdateCustomerRequest;
import com.example.SHOP.dto.response.CustomerListResponse;
import com.example.SHOP.dto.response.CustomerResponse;
import com.example.SHOP.models.CustomerModel;
import com.example.SHOP.services.ICustomerService;
import com.example.SHOP.services.specifications.CustomerSortOrder;

@RestController
@RequestMapping("/")
public class CustomerController {

    @Autowired
    private ICustomerService iCustomerService;

    @GetMapping("customers")
    public ResponseEntity<?> getAllCustomers(
            @RequestParam(required = false) String customerName,
            @RequestParam(required = false) CustomerSortOrder sortOrder,
            @RequestParam(defaultValue = "1") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize) {
        Sort sort;
        if (sortOrder != null) {
            sort = Sort.by(Sort.Order.by(sortOrder.getFieldName()).with(sortOrder.getDirection()));
        } else {
            sort = Sort.by(Sort.Order.by("customerName").with(Sort.Direction.ASC));
        }

        Pageable paging = PageRequest.of(pageNo - 1, pageSize, sort);
        Page<CustomerModel> pageResult;

        if (customerName != null && !customerName.isEmpty()) {
            pageResult = iCustomerService.findCustomersByName(customerName, paging);
            if (pageResult.isEmpty()) {
                return new ResponseEntity<>("Customer name '" + customerName + "' not found", HttpStatus.NOT_FOUND);
            }
        } else {
            pageResult = iCustomerService.findAllCustomers(paging);
        }

        List<CustomerResponse> customerResponses = iCustomerService.convertToCustomerResponse(pageResult);

        ItemsInfo itemsInfo = new ItemsInfo(
                customerResponses != null ? customerResponses.size() : 0,
                (int) pageResult.getTotalElements(),
                pageSize);

        int lastVisiblePage = pageResult.getTotalPages() > 0 ? pageResult.getTotalPages() : 1;

        CustomerPagination paginationInfo = new CustomerPagination(
                pageNo,
                pageResult.hasNext(),
                itemsInfo,
                lastVisiblePage);

        CustomerListResponse response = new CustomerListResponse(customerResponses, paginationInfo);

        if (customerResponses == null || customerResponses.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("customer/{customerId}")
    public ResponseEntity<Object> getCustomerById(@PathVariable Long customerId) {
        CustomerRequest customerRequest = new CustomerRequest(customerId);
        CustomerResponse customers = iCustomerService.findCustomerByCustomerId(customerRequest);
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("customer", customers);
        responseBody.put("message", "Customer with ID " + customerId + "Found");
        return ResponseEntity.ok(responseBody);
    }

    @PostMapping("customer")
    public ResponseEntity<Object> addCustomer(@ModelAttribute AddCustomerRequest addCustomerRequest,
            UriComponentsBuilder uriBuilder) {
        CustomerModel customerModel = iCustomerService.addCustomer(addCustomerRequest);

        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("message", "Customer has been successfully added.");
        responseBody.put("customer", customerModel);

        UriComponents uriComponents = uriBuilder.path("/customer/{customerId}")
                .buildAndExpand(customerModel.getCustomerId());
        return ResponseEntity.created(uriComponents.toUri()).body(responseBody);
    }

    @PutMapping("customer/{customerId}")
    public ResponseEntity<Object> updateCustomer(@PathVariable Long customerId,
            @ModelAttribute UpdateCustomerRequest updateCustomerRequest) {
        CustomerModel updatedCustomer = iCustomerService.updateCustomer(customerId, updateCustomerRequest);

        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("message", "Customer with ID " + customerId + " has been successfully updated.");
        responseBody.put("updatedCustomer", updatedCustomer);

        return ResponseEntity.ok(responseBody);
    }

    @DeleteMapping("customer/{customerId}")
    public ResponseEntity<Object> deleteCustomer(@PathVariable Long customerId) {
        iCustomerService.deleteCustomer(customerId);
        Map<String, Object> responseBody = new HashMap<>();
        responseBody.put("path", "/customer/" + customerId);
        responseBody.put("error", "Success");
        responseBody.put("message", "Customer with ID " + customerId + " has been successfully deleted.");
        responseBody.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        responseBody.put("status", HttpStatus.OK.value());

        return ResponseEntity.ok(responseBody);
    }
}